#include "events.h"
#include <iostream>
#include <vector>
#include <string>
#include <limits>

// Function used in the menu to output options to the user
void outputMenuOptions(){
  std::cout << "Please select an option: " << std::endl;
  std::cout << "0: Exit program" << std::endl;
  std::cout << "1: Add a booking for an event" << std::endl;
  std::cout << "2: Cancel a booking for an event" << std::endl;
  std::cout << "3: List all events" << std::endl;
  std::cout << "4: List details and availability of a given event" << std::endl;
  std::cout << ">> ";
}

// Function used to book an event
void bookEvent(std::vector<Event*> eventVec){
  std::cout << "Which event would you like to buy a ticket: " << std::endl;

  // Variables
  int index = 0;
  int input = 0;
  int seat = 0;

  // Looping through events
  for(Event* e : eventVec){
    std::cout << index << ": ";
    e->printTitleArtist();
    std::cout << std::endl;
    index++;
  }
  std::cout << ">> ";

  // Taking input
  std::cin >> input;

  // If input is not a number
  if(!std::cin){
    // Reset failbit
    std::cin.clear();
    // Skip bad input
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Please enter a numeric value\n" << std::endl;
    return;
  }else if(input < 0 || input >= eventVec.size()){    // If input is out of bounds
    std::cout << "Option out of bounds" << std::endl;
  }else{
    if(eventVec.at(input)->getEventType() == "StandUp"){    // If event is StandUp, ask for seat number
      eventVec.at(input)->printSeatingPlan();
      std::cout << "What seat would you like >> ";
      std::cin >> seat;
      eventVec.at(input)->addBooking(seat);
    }else{    // If event is not StandUp, add booking 
      eventVec.at(input)->addBooking();
    }
  }
  

  std::cout << "\n" << std::endl;
}

// Function used to cancel an event
void cancelEvent(std::vector<Event*> eventVec){
  std::cout << "Which event would you like to cancel a ticket: " << std::endl;

  // Variables
  int index = 0;
  int input = 0;
  int seat = 0;

  // Loop through events
  for(Event* e : eventVec){
    std::cout << index << ": ";
    e->printTitleArtist();
    std::cout << std::endl;
    index++;
  }
  std::cout << ">> ";

  // Take input 
  std::cin >> input;

  // If input is invalid
  if(!std::cin){
    // Reset failbit
    std::cin.clear();
    // Skip bad input
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Please enter a numeric value\n" << std::endl;
    return;
  }else if(input < 0 || input >= eventVec.size()){    // If input is out of bounds
    std::cout << "Option out of bounds" << std::endl;
  }else{
    if(eventVec.at(input)->getEventType() == "StandUp"){    // If event is StandUp, ask for seat number
      std::cout << "What seat would you like to cancel >> ";
      std::cin >> seat;
      eventVec.at(input)->cancelBooking(seat);
    }else{    // If event is not StandUp, cancel booking
      eventVec.at(input)->cancelBooking();
    }
  }
  

  std::cout << "\n" << std::endl;
  
  
  
}

// Function used to list all events, printing just title and artist 
void listAllEvents(std::vector<Event*> eventVec, bool isChoosingEvent){
  // Variables
  int index = 0;

  // Loop through events
  for(Event* e : eventVec){
    // If we are listing events to choose a particular one, print a numbers before it
    if(isChoosingEvent)
      std::cout << index << ": ";
    e->printTitleArtist();
    std::cout << std::endl;
    index++;
  }
  std::cout << std::endl;
}

// Given a particular event, print all details, including capacity and event type
void listGivenEvent(std::vector<Event*> eventVec){
  // Printing all events with option to print numbers before it
  listAllEvents(eventVec, true);

  // Variables
  int index = 0;
  int input = 0;

  std::cout << "Which event would you like to see all the details for >> ";

  // Taking input
  std::cin >> input;

  // If input is invalid
  if(!std::cin){
    // Reset failbit
    std::cin.clear();
    // Skip bad input
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Please enter a numeric value\n" << std::endl;
    return;
  }else if(input < 0 || input >= eventVec.size()){    // If input is out of bounds
    std::cout << "Option out of bounds" << std::endl;
  }else{    // Print all details about the particular event
    eventVec.at(input)->printAllDetails();
  }

  std::cout << std::endl;
  
}

int main(){

  // Creating the vector of events
  // Add events here for testing
  std::vector<Event*> events;
  events.push_back(new LiveMusic("The Keys Tour", "Alicia Keys"));
  events.push_back(new StandUp("Live Innit", "Paul Chowdhry"));
  events.push_back(new Film("Goodfellas", "Martin Scorsese", "2D"));

  // Variables
  bool runProgram = true;
  int input = 0;

  // Menu Loop
  while(runProgram){
    // Printing Menu 
    outputMenuOptions();

    // Storing user input
    std::cin >> input;
    std::cout << std::endl;

    // If input is not a numeric value
    if(!std::cin){
      // Reset failbit
      std::cin.clear();
      // Skip bad input
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Please enter a numeric value\n" << std::endl;
      continue;
    }

    // Handling Input
    switch(input){
    case 1:    // Add a booking for an event
      bookEvent(events);
      break;

    case 2:    // Cancel a booking for an event
      cancelEvent(events);
      break;

    case 3:    // List all events
      listAllEvents(events, false);
      break;

    case 4:    // List a given event
      listGivenEvent(events);
      break;

    case 0:    // Quit program 
      std::cout << "You chose to quit" << std::endl;
      runProgram = false; 
      break;

    default:    // Invalid number
      std::cout << "Please enter a valid number\n" << std::endl;
    }
  }
  
  std::cout << "Goodbye!" << std::endl;
  return 0;
}
