output: main.o events.o
	g++ $^ -o $@

main.o: main.cpp
	g++ -c main.cpp

events.o: events.cpp events.h
	g++ -c events.cpp

.PHONY : clean
clean:
	rm *.o
	rm output
