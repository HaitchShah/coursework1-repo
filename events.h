#ifndef _EVENTS_H_
#define _EVENTS_H_

#include <string>
#include <vector>
#include <iostream>

class Event{
protected:
  // Attributes
  std::string eventType;
  std::string title;
  std::string artist;
  int capacity;
  int max_capacity;

public:
  // Methods
  Event(std::string eventType, std::string title, std::string artist, int capacity);
  virtual ~Event();
  virtual void printTitleArtist();
  virtual void printAllDetails();
  virtual void addBooking();
  virtual void cancelBooking();
  virtual void printSeatingPlan();

  virtual void addBooking(int num);
  virtual void cancelBooking(int num);

  virtual std::string getEventType();
};


class LiveMusic : public Event{
public:
  // Methods
  LiveMusic(std::string title, std::string artist);
  
};


class StandUp : public Event{
protected:
  // Attributes
  std::vector<bool> seating;

public:
  // Methods
  StandUp(std::string title, std::string artist);
  void addBooking(int seatNum);
  void cancelBooking(int seatNum);
  void printSeatingPlan();
  
};


class Film : public Event{
protected:
  // Attributes
  std::string screenFormat;

public:
  // Methods
  Film(std::string title, std::string artist, std::string screenFormat);
  void printAllDetails();

};

#endif
