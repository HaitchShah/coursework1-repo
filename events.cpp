#include "events.h"

// Event Base Class
Event::Event(std::string eventType, std::string title, std::string artist, int capacity){
  this->eventType = eventType;
  this->title = title;
  this->artist = artist;
  this->capacity = capacity;
  this->max_capacity = capacity;
}

Event::~Event(){}

void Event::printTitleArtist(){
  std::cout << "Event Title: " << this->title << std::endl;
  std::cout << "By: " << this->artist << std::endl;
}

void Event::printAllDetails(){
  std::cout << "Event Type: " << this->eventType << std::endl;
  std::cout << "Event Title: " << this->title << std::endl;
  std::cout << "By: " << this->artist << std::endl;
  std::cout << "Capacity Left: " << this->capacity << std::endl;
}

void Event::addBooking(){
  // If there is enough capacity
  if(this->capacity > 0){
    this->capacity--;

    std::cout << "Event Booked! Please give ticket to customer." << std::endl;
  }else{
    std::cout << "Event Sold Out! Please choose another event." << std::endl;
  }
}

void Event::cancelBooking(){
  // If capacity is still at max
  if(this->capacity == this->max_capacity){
    std::cout << "No Tickets To Refund!" << std::endl;
  }else{
    this->capacity++;
    std::cout << "Ticket Refunded!" << std::endl;
  }
}

void Event::printSeatingPlan(){}

void Event::addBooking(int num){}

void Event::cancelBooking(int num){}

std::string Event::getEventType(){ return this->eventType; }



// LiveMusic Derived Class
LiveMusic::LiveMusic(std::string title, std::string artist) : Event("LiveMusic", title, artist, 300){}



// StandUp Derived Class
StandUp::StandUp(std::string title, std::string artist) : Event("StandUp", title, artist, 200){
  this->seating.assign(200, false);
}

void StandUp::addBooking(int seatNum){
  // Actual variable that will be used on vector
  int seat = seatNum - 1;

  if(seat < 0 || seat > 199){    // If seat is out of bounds
    std::cout << "Please select a seat from 1-200." << std::endl;
  }else if(this->seating.at(seat) == true){    // If seat is taken
    std::cout << "Seat is taken. Please select another." << std::endl;
  }else{    // If seat is in bounds and not taken
    this->seating.at(seat) = true;
    this->capacity--;
    std::cout << "Ticket Booked! Your Seat No. is " << seatNum << "." << std::endl;
  }
}

void StandUp::cancelBooking(int seatNum){
  // Actual variable that will be used on vector
  int seat = seatNum - 1;

  if(seat < 0 || seat > 199){    // If seat is out of bounds
    std::cout << "Please select a seat from 1-200." << std::endl;
  }else if(this->seating.at(seat) == false){    // If seat is not taken
    std::cout << "Seat has not been taken. Must be incorrect seat num." << std::endl;
  }else{    // If seat is in bounds and taken
    this->seating.at(seat) = false;
    this->capacity++;
    std::cout << "Ticket Refunded!" << std::endl;
  }
}

void StandUp::printSeatingPlan(){
  std::cout << "Here are the seats that are available.\nX marks a seat that is unavailable." << std::endl;

  int index = 1;
  int newlineIndex = 1;

  // Looping through seating vector
  for(int i = 0; i < this->seating.size(); i++){
    if(this->seating.at(i) == false){
      std::cout << i+1 << "  ";
    }else{
      std::cout << "x" << "  ";
    }

    // If we have printed 20 seats, go to newline
    if(newlineIndex == 20){
      std::cout << std::endl;
      newlineIndex = 1;
    }else{
      newlineIndex++;
    }
  }

  
}



// Film Derived Class
Film::Film(std::string title, std::string artist, std::string screenFormat) : Event("Film", title, artist, 200){
  this->screenFormat = screenFormat;
}

void Film::printAllDetails(){
  std::cout << "Event Type: " << this->eventType << std::endl;
  std::cout << "Event Title: " << this->title << std::endl;
  std::cout << "By: " << this->artist << std::endl;
  std::cout << "Format: " << this->screenFormat << std::endl;
  std::cout << "Capacity Left: " << this->capacity << std::endl;
}
